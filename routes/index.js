const { json } = require('express');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var users = [];

//Confirmar se a API está funcionando
router.get('/health', (req, res) => {
  return res.json({message: 'Aplicação está funcionando!'});
  });

//Cadastrar novos usuários
router.post('/cadastro',  (req, res) => {
  const { nome, email, senha, idade } = req.body; 

  const user = {
    nome,
    email,
    senha,
    idade
  };
  users.push(user);
  return res.json(user); 
})

router.post('/login',  (req, res) => {
  const { email, senha } = req.body; 

  const userLogin = {
    email,
    senha
  };

  var existe;
  
  //Validação do usuário
  users.forEach(userArray => {
    if (userArray.email == userLogin.email && userArray.senha == userLogin.senha) {
      existe = true;
    } 
  });

  if (existe == true) {
    return res.json({result : "Credenciais válidas", loginSuccess : true});
  } else {
    return res.json({result : "Email ou senha incorreto", loginSuccess : false});
  }

})

module.exports = router;


